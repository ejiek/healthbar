/*
healtbar
Copyright (C) 2018  Vladislav Petrov
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs::File;
use std::io::ErrorKind;
use std::io::prelude::*;
use std::fmt::Write;
use std::process::Command;

use color::Color;
mod color;

#[derive(Debug)]
enum BatStatus {
    Charging,
    Discharging,
    Full,
    Unknown,
}

fn main() {
    let charge = get_charge();
    let level = (charge / 20.0).trunc() as u8;
    let sub_level = charge - (level as f64) * 20.0;

    let full = Color::parse("#ff0000"); // single heart
    let empty_discharging = Color::parse("#404040");
    let empty_charging = Color::parse("#90C3D4");
    let charged = Color::parse("#8FD989"); // whole battery

    let health_bar = match get_status() {
        BatStatus::Discharging => form_line(empty_discharging, full, level, sub_level),
        BatStatus::Full => form_line(charged, charged, level, sub_level),
        BatStatus::Charging => form_line(empty_charging, full, level, sub_level),
        BatStatus::Unknown => form_line(empty_discharging, full, level, sub_level),
    };

    Command::new("dunstify")
        .arg("--appname=healthbar")
        .arg("-r")
        .arg("2085")
        .arg(" ") // empty title to get health_bar to the actual body
        .arg(health_bar)
        .spawn()
        .expect("failed to execute process");
}

fn get_charge() -> f64 {
    let f_full = File::open("/sys/class/power_supply/BAT0/charge_full");

    let mut f_full = match f_full {
        Ok(file) => file,
        Err(ref error) if error.kind() == ErrorKind::NotFound => {
            match File::open("/sys/class/power_supply/BAT0/energy_full") {
                Ok(file) => file,
                Err(error) => panic!("There was a problem opening the file: {:?}", error),
            }
        },
        Err(error) => panic!("There was a problem opening the file: {:?}", error),
    };

    let f_now = File::open("/sys/class/power_supply/BAT0/charge_now");

    let mut f_now = match f_now {
        Ok(file) => file,
        Err(ref error) if error.kind() == ErrorKind::NotFound => {
            match File::open("/sys/class/power_supply/BAT0/energy_now") {
                Ok(file) => file,
                Err(error) => panic!("There was a problem opening the file: {:?}", error),
            }
        },
        Err(error) => panic!("There was a problem opening the file: {:?}", error),
    };

    let mut full = String::new();
    let mut now = String::new();
    f_full
        .read_to_string(&mut full)
        .expect("something went wrong reading the file");
    f_now
        .read_to_string(&mut now)
        .expect("something went wrong reading the file");

    full = full.trim().parse().expect("Wanted a number");

    now = now.trim().parse().expect("Wanted a number");

    now.parse::<f64>().unwrap() / full.parse::<f64>().unwrap() * 100.0
}

fn get_status() -> BatStatus {
    let mut f_status = File::open("/sys/class/power_supply/BAT0/status").expect("file not found");
    let mut status = String::new();
    f_status
        .read_to_string(&mut status)
        .expect("something went wrong reading the file");

    match status.trim() {
        "Discharging" => BatStatus::Discharging,
        "Charging" => BatStatus::Charging,
        "Full" => BatStatus::Full,
        _ => BatStatus::Unknown,
    }
}

fn form_line(low: Color, high: Color, level: u8, sub_level: f64) -> String {
    let mut health = String::new();
    for n in 0..5 {
        if n < level {
            add_heart(&mut health, &high);
        } else if n == level {
            add_heart(&mut health, &low.gradient(&high, sub_level));
        } else if n > level {
            add_heart(&mut health, &low);
        }
    }
    health
}

fn add_heart(mut health: &mut String, color: &Color) {
    write!(
        &mut health,
        "<span font='Font Awesome 5 Free Solid 16' color='{}'>  </span>",
        color
    ).unwrap();
}
