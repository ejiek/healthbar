/*
healtbar
Copyright (C) 2018  Vladislav Petrov
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fmt;

#[derive(Clone, Copy)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {
    pub fn new(red_: u8, green_: u8, blue_: u8) -> Color {
        Color {
            red: red_,
            green: green_,
            blue: blue_,
        }
    }

    pub fn parse(string: &str) -> Color {
        Color {
            blue: u8::from_str_radix(&string[5..7], 16).unwrap(),
            green: u8::from_str_radix(&string[3..5], 16).unwrap(),
            red: u8::from_str_radix(&string[1..3], 16).unwrap(),
        }
    }

    pub fn gradient(&self, other: &Color, level: f64) -> Color {
        Color {
            red: Color::gradient_channel(self.red, other.red, level),
            green: Color::gradient_channel(self.green, other.green, level),
            blue: Color::gradient_channel(self.blue, other.blue, level),
        }
    }

    fn gradient_channel(channel_1: u8, channel_2: u8, level: f64) -> u8 {
        ((channel_1 as f64) - level * (((channel_1 as i16) - (channel_2 as i16)) as f64) / 20.0)
            .round() as u8
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "#{:0>2X}{:0>2X}{:0>2X}", self.red, self.green, self.blue)
    }
}
